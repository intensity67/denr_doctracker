 var transactions_item_id, 
 product_item_id, 
 product_price, 
 product_name_var,
 trans_item_id,
 prod_name_restock_var;

  function transaction_item_id_change(id){

      transactions_item_id = id;

  }
  
  function product_name_change(product_name){

    product_name_var = product_name;
  }

  function restock_product_change(id, prod_name_restock){
     product_item_id = id;
    prod_name_restock_var = prod_name_restock;


  }

  function product_item_id_change(id){

      product_item_id = id;

  }

  function trans_item_id_change(id){

       trans_item_id = id;

  }

  function product_price_change(product_price_change, id){

      product_price = product_price_change;

      product_item_id = id;

  }


    $("document").ready(function(){
      
      $(".order_btn").click(function(){

          $("#product_id").val($(this).attr('id'));

          $("#product_name_desc").html(product_name_var);
          
          $("#cust_bill").attr("autofocus");

          $( "#cust_bill" ).attr( "autofocus" ) ;

          //* $("#order_qty").focus();
 
        });

       $(".trans_backtrack").click(function(){

          $("#trans_id").val(transactions_item_id);

       });


       $(".on_sale").click(function(){
        
          $("input[name='productId']").val(product_item_id);

       });
        

       $("#billout_btn").click(function(){

           $( "#cust_bill" ).attr( "autofocus");
           $( "#cust_bill" ).prop( "autofocus");

       });


        $(".change_price").click(function(){

          $("input[name='price_change']").val(product_price);
            
          $("input[name='productId']").val(product_item_id);

        }); 

        $("#mark_defect").click(function(){

          $("#trans_item_id").val(trans_item_id);

        });


        $("#credit_card_form").hide();

 
        $("#payment_type").change(function(){

          if($(this).val() == "cash"){

            $("#credit_card_form").hide();

          }

          if($(this).val() == "credit_card"){
          
            $("#credit_card_form").show();

          }

        });

        $("#promo_tot_payment").hide();

        
        $("#sale_type").change(function(){

          if($(this).val() == "normal"){

            $("#promo_tot_payment").hide();

          }

          if($(this).val() == "promo"){
          
            $("#promo_tot_payment").show();

          }

        });

        $("#replacement_form").hide();

        $("#defect_action").change(function(){

          if($(this).val() == "cash_back"){

            $("#replacement_form").hide();

          }

          if($(this).val() == "replacement"){
          
              $("#replacement_form").show();

          }

        });

 
      $(".cancel_order_btn").click(function(){

             $("#transactions_item_id").val(transactions_item_id);
       });

      $(".restocks_prod").click(function(){


             $("#prod_name_restock").text(prod_name_restock_var);

 
             $("input[name='productIdRestocks']").val(product_item_id);
       });


      $(".remove_prod_btn").click(function(){

             $("#product_item_id").val(product_item_id);
       });


            $("#promo_form").hide();
            $("#package_form").hide();
            $("#customer_bill_exception").hide();

      $("#exception_type").change(function(){
             

            if( $(this).val() == 'freebies') {

              $("#promo_form").hide();
              $("#customer_bill_exception").hide();


            }



            if( $(this).val() == 'promo') {

                 $("#promo_form").show();

                 $("#customer_bill_exception").show();

                 $("#package_form").hide();

            }



            if( $(this).val() == 'package') {

                 $("#package_form").show();

                 $("#customer_bill_exception").show();

                 $("#promo_form").hide();

            }

   
      });

    });