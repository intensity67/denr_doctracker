-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2018 at 01:31 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `denr_doc_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `acct_id` int(11) NOT NULL,
  `acct_username` varchar(255) NOT NULL,
  `acct_password` varchar(255) NOT NULL,
  `acct_name` varchar(255) NOT NULL,
  `acct_type_id` int(11) NOT NULL,
  `acct_dept_id` int(11) NOT NULL,
  `acct_section_id` int(11) DEFAULT NULL,
  `acct_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`acct_id`, `acct_username`, `acct_password`, `acct_name`, `acct_type_id`, `acct_dept_id`, `acct_section_id`, `acct_date_created`) VALUES
(1, 'help_desk123', '2ada96087454c8c5695a23ec37bd61a8', 'Gen Tagalog', 1, 1, 0, '2018-04-22 05:16:21'),
(2, 'management_staff123', '202cb962ac59075b964b07152d234b70', 'Clark Enriquez', 2, 1, 0, '2017-12-23 09:52:27'),
(3, 'technical_staff', '202cb962ac59075b964b07152d234b70', 'Technician Dude', 3, 1, 0, '2017-12-23 09:23:33'),
(4, 'admin_123', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin', 4, 1, 0, '2018-04-26 22:46:56'),
(5, 'Penro', '96e89a298e0a9f469b9ae458d6afae9f', 'Penro Admin', 5, 1, 0, '2018-04-27 00:05:29');

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `account_types_id` int(11) NOT NULL,
  `account_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`account_types_id`, `account_type`) VALUES
(1, 'Help Desk Staff'),
(2, 'Management Staff'),
(3, 'Technical Staff'),
(4, 'Admin'),
(5, 'Head');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `department_desc` varchar(255) NOT NULL,
  `department_status` enum('Active','Deleted') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_name`, `department_desc`, `department_status`) VALUES
(1, 'Management ', '', 'Active'),
(2, 'Technical ', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `document_entry`
--

CREATE TABLE `document_entry` (
  `doc_entry_id` int(11) NOT NULL,
  `doc_subject` varchar(255) NOT NULL,
  `doc_no` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `client_addr` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `corporation` varchar(255) NOT NULL,
  `date_received` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `document_status_id` int(11) NOT NULL DEFAULT '1',
  `document_location_id` int(11) NOT NULL,
  `current_remarks` varchar(255) DEFAULT NULL,
  `signed_status` enum('unsigned','signed') NOT NULL,
  `signed_by` varchar(255) DEFAULT NULL,
  `date_returned` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` varchar(255) DEFAULT NULL,
  `forwarded_dept_id` int(11) NOT NULL,
  `forwarded_sec_id` int(11) NOT NULL,
  `rec_status` enum('Active','Deleted','Pending') NOT NULL,
  `for_status` int(11) DEFAULT NULL,
  `confirmation_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_entry`
--

INSERT INTO `document_entry` (`doc_entry_id`, `doc_subject`, `doc_no`, `first_name`, `last_name`, `middle_name`, `client_addr`, `phone_number`, `corporation`, `date_received`, `document_status_id`, `document_location_id`, `current_remarks`, `signed_status`, `signed_by`, `date_returned`, `notes`, `forwarded_dept_id`, `forwarded_sec_id`, `rec_status`, `for_status`, `confirmation_status`) VALUES
(1, 'Docmentation', 321, 'Gen ', 'Tagalog', 'unknown', 'Canduman, Mandaue City	', '1029312', 'Intenzitia', '2018-04-28 09:24:48', 2, 3, 'fdxfv', 'signed', 'ADA', '0000-00-00 00:00:00', '', 0, 1, 'Active', 0, 1),
(2, '', 0, 'John ', 'Doe ', '', 'Mandaue City ', '3883888', '', '2018-04-28 01:51:57', 2, 2, '', 'signed', 'dsada', '2017-12-14 16:00:00', 'asdas', 0, 6, 'Active', 0, 0),
(3, '', 0, 'adasdada', '', '', 'asdasda', '09112313', '', '2018-04-28 01:52:05', 2, 1, '', 'unsigned', NULL, '0000-00-00 00:00:00', NULL, 0, 1, 'Active', 0, 0),
(4, '', 0, 'adasdada', '', '', 'asdasda', '09112313', '', '2018-04-28 01:52:12', 1, 1, '', 'unsigned', NULL, '0000-00-00 00:00:00', NULL, 0, 1, 'Active', 0, 0),
(5, '', 0, 'DocumentSampleCLient', '', '', 'asd', '09112313', '', '2018-04-28 01:52:16', 5, 1, '', 'signed', 'asdasd', '2018-01-06 16:00:00', 'hkhkj', 1, 4, 'Active', 0, 0),
(6, 'SADA', 0, 'jk', 'dasdsa', '', 'asd', '909123', '', '2018-04-28 01:52:20', 1, 1, '', 'unsigned', NULL, '0000-00-00 00:00:00', NULL, 1, 1, 'Active', 0, 0),
(7, 'SADA', 0, 'jk', 'dasdsa', 'jk', 'H. Abellana St.,', '98090', '', '2018-04-28 01:52:24', 1, 0, NULL, 'unsigned', NULL, '0000-00-00 00:00:00', NULL, 1, 1, 'Active', 0, 0),
(8, 'TEST', 2222, 'ansel', 'lim', 'bantilan', 'kinasang -an', '09422326496', 'Intenzitia', '2018-04-28 09:52:16', 1, 7, 'dddd', 'unsigned', NULL, '0000-00-00 00:00:00', NULL, 1, 5, 'Active', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `document_locations`
--

CREATE TABLE `document_locations` (
  `document_location_id` int(11) NOT NULL,
  `document_location` varchar(255) NOT NULL,
  `timestamp_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_locations`
--

INSERT INTO `document_locations` (`document_location_id`, `document_location`, `timestamp_added`) VALUES
(1, 'Helpdesk', '2018-02-12 07:54:39'),
(2, 'Record Section', '2018-02-12 07:54:39'),
(3, 'Finance Section', '2018-02-12 07:54:39'),
(4, 'Planning Section', '2018-02-12 07:54:39'),
(5, 'General Services', '2018-02-12 07:54:39'),
(6, 'Cashier Section', '2018-02-12 07:54:39'),
(7, 'LEDS', '2018-02-12 07:54:39'),
(8, 'Management ', '2018-02-12 07:54:39'),
(9, 'Technical ', '2018-02-12 07:54:39');

-- --------------------------------------------------------

--
-- Table structure for table `document_status`
--

CREATE TABLE `document_status` (
  `document_status_id` int(11) NOT NULL,
  `document_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_status`
--

INSERT INTO `document_status` (`document_status_id`, `document_status`) VALUES
(1, 'Received by Helpdesk'),
(2, 'Forwarded to Department'),
(3, 'Forwarded to Section'),
(4, 'Received Back in Department'),
(5, 'Received Back in Helpdesk');

-- --------------------------------------------------------

--
-- Table structure for table `doc_logs`
--

CREATE TABLE `doc_logs` (
  `doc_logs_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `doc_location_id` int(11) NOT NULL,
  `forward_remarks` varchar(255) NOT NULL,
  `passed_by_id` int(11) NOT NULL,
  `received_by_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doc_logs`
--

INSERT INTO `doc_logs` (`doc_logs_id`, `doc_id`, `doc_location_id`, `forward_remarks`, `passed_by_id`, `received_by_id`, `timestamp`, `status`) VALUES
(1, 2, 4, '', 4, NULL, '2018-02-14 13:40:37', 0),
(2, 1, 2, '', 4, NULL, '2018-02-14 13:40:42', 0),
(3, 2, 2, '', 4, NULL, '2018-02-14 13:41:51', 0),
(4, 1, 1, 'sample remarks', 4, NULL, '2018-02-14 14:14:41', 0),
(5, 1, 1, 'remarks 2', 4, NULL, '2018-02-14 14:16:44', 0),
(6, 1, 4, '', 4, NULL, '2018-02-14 14:17:09', 0),
(7, 2, 2, '', 4, NULL, '2018-02-19 04:31:05', 0),
(8, 2, 2, '', 4, NULL, '2018-02-19 04:31:05', 0),
(9, 1, 5, 'SMAPLE REMARKS', 4, NULL, '2018-02-26 11:47:57', 0),
(10, 1, 3, 'fdxfv', 4, NULL, '2018-04-22 05:59:08', 0),
(11, 8, 1, 'sign', 4, NULL, '2018-04-27 08:17:20', 0),
(12, 8, 7, 'dddd', 4, NULL, '2018-04-27 08:34:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `section_desc` varchar(255) NOT NULL,
  `section_stat` enum('Active','Deleted') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `department_id`, `section_name`, `section_desc`, `section_stat`) VALUES
(1, 1, 'Record Section', '', 'Active'),
(2, 1, 'Finance Section', '', 'Active'),
(3, 1, 'Planning Section', '', 'Active'),
(4, 1, 'General Services', '', 'Active'),
(5, 1, 'Cashier Section', '', 'Active'),
(6, 2, 'LEDS', 'LICENSE PATENTS AND PERMIT SECTION', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`acct_id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`account_types_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `document_entry`
--
ALTER TABLE `document_entry`
  ADD PRIMARY KEY (`doc_entry_id`);

--
-- Indexes for table `document_locations`
--
ALTER TABLE `document_locations`
  ADD PRIMARY KEY (`document_location_id`);

--
-- Indexes for table `document_status`
--
ALTER TABLE `document_status`
  ADD PRIMARY KEY (`document_status_id`);

--
-- Indexes for table `doc_logs`
--
ALTER TABLE `doc_logs`
  ADD PRIMARY KEY (`doc_logs_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `acct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `account_types_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `document_entry`
--
ALTER TABLE `document_entry`
  MODIFY `doc_entry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `document_locations`
--
ALTER TABLE `document_locations`
  MODIFY `document_location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `document_status`
--
ALTER TABLE `document_status`
  MODIFY `document_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `doc_logs`
--
ALTER TABLE `doc_logs`
  MODIFY `doc_logs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
