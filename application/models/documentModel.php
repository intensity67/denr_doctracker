<?php
 
	class documentModel extends CI_Model{


        public function __construct(){

                // Call the CI_Model constructor
                parent::__construct();
        }


        public function get_doc_lists(){

                $this->db->join('departments', 'departments.department_id = sections.department_id');

                $this->db->where("section_stat", "Active");
                
                $query = $this->db->get('sections');
                
                return $query->result_array();
        }


        public function get_document_locations(){

                $query = $this->db->get('document_locations');
                
                return $query->result_array();
        }

        public function get_doc_forward_logs($document_id){

                $this->db->join('document_locations', ' document_locations.document_location_id = doc_logs.doc_location_id');

                $this->db->join('accounts', 'doc_logs.passed_by_id = accounts.acct_id');

                $this->db->join('account_types', 'account_types.account_types_id = accounts.acct_type_id');
                
                $this->db->order_by("doc_logs_id", "desc");

                 $this->db->where("doc_id", $document_id);
                
                $query = $this->db->get('doc_logs');
                
                return $query->result_array();
        }

        public function get_section_lists(){

                $this->db->join('departments', 'departments.department_id = sections.department_id');

                $this->db->where("section_stat", "Active");
                
                $query = $this->db->get('sections');
                
                return $query->result_array();
        }

        public function get_section_info($section_id){

                $this->db->join('departments', 'departments.department_id = sections.department_id');

                $this->db->where("section_stat", "Active");

                $this->db->where("section_id", $section_id);
                
                $query = $this->db->get('sections');
                
                return $query->row_array();
        }

        public function get_department_lists(){

                $this->db->where("department_status", "Active");
                
                $query = $this->db->get('departments');
                
                return $query->result_array();
        }

        public function get_entry_lists($date_filters = ''){

                // $this->db->join('sections', 'sections.section_id = document_entry.doc_entry_id');

                // $this->db->join('document_status', 'document_status.document_status_id = document_entry.document_status_id', 'right');

                $this->db->join('document_locations', 'document_locations.document_location_id = document_entry.document_location_id', 'left');

                $this->db->where("rec_status", "Active");

                if(!empty($date_filters)){

                  $this->db->where('date_received >=', $date_filters['date_from']);

                  $this->db->where('date_received <=', $date_filters['date_to']);

                }
                
                $query = $this->db->get('document_entry');
                
                return $query->result_array();
        }

        public function insert_document_entry($document_entry_data){

             
              $this->db->insert('document_entry', $document_entry_data);

              return $this->db->insert_id();
        }

        public function insert_doc_forward_logs($forward_doc_log_data){

             
              $this->db->insert('doc_logs', $forward_doc_log_data);

              return $this->db->insert_id();
        }

        
        public function forward_document($document_id, $forward_doc_data){

              $this->db->set($forward_doc_data);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }       
        public function forward_document_to_department($document_id){

              $this->db->set('document_status_id ',  FORWARDED_DEPARTMENT);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }       
        
        public function forward_document_to_section($document_id){

              $this->db->set('document_status_id ',  FORWARDED_SECTION);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }       

        public function received_back_department($document_id, $updated_fields){

              $this->db->set($updated_fields);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }       

        public function received_back_helpdesk($document_id){

              $this->db->set('document_status_id ',  RECEIVED_BACK_HELPDESK);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }

        public function update_sample($document_id){

              $this->db->set('document_status_id ',  FORWARDED_DEPARTMENT);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }       

     	public function delete_user($acct_id){

                $this->db->where("acct_id", $acct_id);
                $this->db->delete('accounts');

        }
        // 
          //select counts of request manager

      public function notify_doc(){ 

        $this->db->select('*')->from('document_entry')->where('for_status', 1);
        $query=$this->db->get();
        return $query->num_rows();
        $db->close();
      }
      public function update_status($status){
              $this->db->set('for_status ',  0);
              $this->db->where('for_status',    $status);
              $this->db->update('document_entry');

        } 
      public function update_confirmation($document_id, $confirmation_status){
              $this->db->set('confirmation_status', $confirmation_status);
              $this->db->where('doc_entry_id',    $document_id);
              $this->db->update('document_entry');

        }  



	}


?>