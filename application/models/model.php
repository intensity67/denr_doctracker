<?php
 
	class Model extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }


        public function get_products_all()
        {

                $this->db->where("recstat", "active");
                $query = $this->db->get('products');
                return $query->result();
        }

        
        public function update_catname($cat_id, $cat_name)

        {
               $this->db->set('category_name ',  $cat_name);
              $this->db->where('category_id',    $cat_id);
              $this->db->update('category');
        }       

        public function insert_products($prod_data)
        {
             
              $this->db->insert('products', $prod_data);

              return $this->db->insert_id();
        }

     	public function delete_user($acct_id)

        {
                $this->db->where("acct_id", $acct_id);
                $this->db->delete('account');
        }



	}


?>