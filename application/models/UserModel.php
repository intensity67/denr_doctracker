<?php
 
  class UserModel extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        // add User Account
      public function insertaccount($data) {
         if ($this->db->insert("accounts", $data)) {
            return true;
         }
      }
     //  view all users
        public function get_user_lists(){
        $get = $this->db->query("SELECT * FROM accounts 
            JOIN account_types ON accounts.acct_type_id = account_types.account_types_id
            JOIN departments ON accounts.acct_dept_id = departments.department_id");
        return $get->result_array();
        }
        // get user types
        public function get_usertype(){
        $get = $this->db->query("SELECT * FROM  account_types");
        return $get->result_array();
        }

  }


?>