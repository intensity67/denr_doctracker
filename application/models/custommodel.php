<?php
	class custommodel extends CI_Model{

	public function getmyrec($dentistid){

		$sqlstr = "SELECT * FROM dentist WHERE d_acctid = '$dentistid'";
		$get = $this->db->query($sqlstr);

		return $get->result();
	}

	public function GetPatient(){

		$get = $this->db->query("SELECT * FROM account JOIN patient ON account.userid = patient.p_accntID WHERE patient.p_status = 'waiting'");
		
		return $get->result();
	}

	public function Records(){

		$get = $this->db->query("SELECT * FROM account JOIN patient ON account.userid = patient.p_accntID WHERE patient.p_status = 'accept'");
		
		return $get->result();
	}

	public function patient_info($patient_id){

		$get = $this->db->query("SELECT * FROM account JOIN patient ON account.userid = patient.p_accntID WHERE patient_id = '$patient_id'");
		
		return $get->result();
	}

	public function get_services(){

		$get = $this->db->query("SELECT * FROM  service  ");
		
		return $get->result_array();
	}


	public function get_patient(){

		$get = $this->db->query("SELECT * FROM  patient  ");
		
		return $get->result_array();
	}

	public function get_all_appointments(){

		$get = $this->db->query("SELECT * FROM  appointment 
								JOIN service ON service.service_id =  appointment.service_id
								JOIN patient ON patient.patient_id =  appointment.patient_id
  								");
		
		return $get->result_array();
	}

	public function get_pending_appoinments(){

		$get = $this->db->query("SELECT * FROM  appointment 

								JOIN service ON service.service_id =  appointment.service_id
								JOIN patient ON patient.patient_id =  appointment.patient_id

								WHERE status = 'Pending' ");
		
		return $get->result_array();
	}
	
	public function approve_appointment($appointment_id){

		$get = "UPDATE appointment SET status = 'Approve' WHERE appointment_id = '$appointment_id'";
	
		$this->db->query($get);
		
	}
		
	public function mark_as_done($appointment_id){

		$get = "UPDATE appointment SET status = 'Done' WHERE appointment_id = '$appointment_id'";
	
		$this->db->query($get);
		
	}

	public function get_approved_appoinments(){

		$get = $this->db->query("SELECT * FROM  appointment

								JOIN service ON service.service_id =  appointment.service_id
								JOIN patient ON patient.patient_id =  appointment.patient_id

								WHERE status = 'Approve' ");
		
		return $get->result_array();
	}

	public function get_appoinments_history(){

		$get = $this->db->query("SELECT * FROM appointment

								JOIN service ON service.service_id =  appointment.service_id
								JOIN patient ON patient.patient_id =  appointment.patient_id

								WHERE status = 'Done' ");
		
		return $get->result_array();
	}

	public function Viewpatient($id){

		$get = $this->db->query("SELECT * FROM account JOIN patient ON account.userid = patient.p_accntID
			WHERE patient.patient_id = '$id'");
		
		return $get->result();
	}


	public function teethpatient($id){

		$get = $this->db->query("SELECT * FROM account JOIN patient ON account.userid = patient.p_accntID
			WHERE patient.patient_id = '$id'");
		
		return $get->result();
	}

	public function acceptpatient($id, $p_status){

		$get = "UPDATE patient SET p_status = '$p_status' WHERE p_accntID = '$id'";
	
		$this->db->query($get);
		

	}


	public function RecordTooth($patient_id, $tooth, $status){

		$this->db->query("INSERT INTO teeth_record VALUES('','$patient_id', '$tooth','NULL', '$status' )");
	}

	public function insert_patient($data){

			$this->db->insert('patient', $data);

			return $this->db->insert_id();
			
 	}
 	
	public function insert_appointment($data){

			$this->db->insert('appointment', $data);

			return $this->db->insert_id();
			
 	}

	public function insert_account($data){

			$this->db->insert('account', $data); 

			return $this->db->insert_id();

 	}

	public function insert_appointment_remark($remark, $appointment_id){


		$get = "UPDATE appointment SET remark= '$remark' WHERE appointment_id = '$appointment_id' ";
	
		$this->db->query($get);

 	}

	public function get_patients(){

		$sqlstr = "SELECT * FROM  patient ";
		$get = $this->db->query($sqlstr);

		return $get->result_array();
	}

	public function get_appointment_info($appointment_id){

		$sqlstr = "SELECT * FROM  appointment 

			JOIN patient ON patient.patient_id = appointment.patient_id

			JOIN service ON appointment.service_id = service.service_id

			WHERE appointment.appointment_id = '$appointment_id'  ";

		$get = $this->db->query($sqlstr);

		return $get->row_array();
	}

	public function get_teeth_patient($patient_id){

		$sqlstr = "SELECT * FROM  teeth_record WHERE patient_id=  '$patient_id' ";

		$get = $this->db->query($sqlstr);

		return $get->result_array();
	}

	}
?>