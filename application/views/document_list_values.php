
				<?php foreach($doc_entry_list as $row): ?>

					<tr>

						<td> <?php echo $row['doc_entry_id']; ?> 

						<td> <?php echo $row['doc_subject']; ?>
						<td> <?php echo $row['doc_no']; ?>

						<td> <?php echo date("M d, Y", strtotime($row['date_received'])); ?> 

						<td> <?php echo $row['last_name']; ?>

						<td> <?php echo $row['middle_name']; ?> 

						<td> <?php echo $row['first_name']; ?> 

						<td> <?php echo $row['client_addr']; ?> 

						<td> <?php echo $row['phone_number']; ?> 
						<td> <?php echo $row['corporation']; ?> 
 
 						<td> <?php echo $row['signed_status']; ?>

						<td> <?php echo $row['document_location']; ?>

  
 						<td>  
  					
 
							<a href="#" class="btn btn-success forward_to_modal_btn" data-toggle = "modal" data-target = '#forward_to_modal'  value= "<?php echo $row['doc_entry_id']; ?>"> 

									Forwarded Document <i class="fa fa-mail-forward"> </i> 

							</a>
							
							<a href="#" class="btn btn-success forward_to_modal_btn btn-info btn-mini" data-toggle = "modal" data-target = "#doc_forward_logs<?php echo $row['doc_entry_id']; ?>"  value= "<?php echo $row['doc_entry_id']; ?>"> 

									Forward Logs <i class="fa fa-eye"> </i> 

							</a>
							
 						
						<br><br>

							<a href="#"  data-toggle = "modal" data-target = "#edit_data_entry_modal<?php echo $row['doc_entry_id']; ?>" class="btn btn-success"> 
								<i class="fa fa-edit"></i> 
							</a>

							<a href="" class="btn btn-danger"> <i class="fa fa-trash"></i>  </a>
						
						<?php $row['section_list']	=	$section_list; ?>

						<?php $this->load->view("modals/edit_entry_modal", $row); ?>

						<?php $doc_history_modal_data['doc_forward_logs']	=	$this->documentModel->get_doc_forward_logs($row['doc_entry_id']); ?>

						<?php $this->load->view("modals/doc_history_modal", $doc_history_modal_data); ?>
	  				
				<?php endforeach; ?>
