
<?php
 		require 'fpdf_header.php';
 		
		$pdf = new PDF('P','mm', array( 58 , 210 ));

		//$pdf = new PDF('P','mm', array( 300 , 300 ));

 		//new FPDF('P','mm',array(100,150));

 		require 'dbconnect.php';
 		
 		$order_id = $_GET['order_id'];


 		$order_info_query = mysqli_query($conn, "SELECT * FROM orders WHERE order_id = '$order_id' ");
			
 		$order_info = mysqli_fetch_assoc($order_info_query);

		$orders_query= mysqli_query($conn, "SELECT * FROM order_details WHERE fk_order_id = '$order_id' ");

 		$i= 0;

		$pdf->order_id 		= $order_id;
		$pdf->table_no 		= $order_info['table_no'];
		$pdf->date_ordered 	= date("M, d Y h:i A", strtotime($order_info['date_timein']));
		$pdf->no_persons 	= $order_info['no_of_people'];


		$pdf->AddPage();

		while($row = mysqli_fetch_assoc($orders_query)){

 			$orders_row[$i]['nos'] 			=	$i + 1;		
 			$orders_row[$i]['description'] 	=	$row['description'];		
 			$orders_row[$i]['qty'] 			=	$row['qty'];		
			
			$i++;
				
		}
 
			
		//*For Background: $pdf->Image('img/background.png',0,15,-175);
		
		if(!empty($orders_row))

			$pdf->BasicTable($orders_row);

		else 

			$pdf->EmptyInventory();

		$pdf->SetFont('Arial','', 5);

		$pdf->Cell(0, 5,'Date Printed:  '. date("M d, Y"));
 
		$pdf->SetTitle('View Order Print ');
		
		$pdf->Output();
		
		//$pdf->Output('sample_filel.pdf', 'F');

?>