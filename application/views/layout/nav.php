<?php
if ($this->session->userdata('account_info')['acct_id'] == 5) { ?>
<div class="navbar-header" >
    <a class="navbar-brand" href="<?php echo base_url(''); ?>"> DENR DOCUMENT TRACKER </a>
        <p class="navbar-brand"> (Login As:  Admin) </p>
    </div>
            <div class="navbar-default sidebar" role="navigation">

                <div class="sidebar-nav navbar-collapse" >

                    <ul class="nav" id="side-menu" >


                        <li><a href = "<?php echo base_url('HeadController/dashboard'); ?>"> Dashboard </a></li>
                        <li><a class="get_notify" href = "<?php echo base_url('HeadController/document_lists'); ?>"> Document Lists <span class="request badge" style="background-color:#ad0c0c; color:#fcf8e3;"></a></li>
                        <li><a href = "<?php echo base_url('HeadController/department'); ?>">  Departments  <span class="fa arrow"></span> </a>
                                <ul class="nav nav-second-level">
                                    <li>       
                                        <a href= "<?php echo base_url('HeadController/departments_lists'); ?>">  <i class = 'fa fa-list'> </i> Departments Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('HeadController/section'); ?>">  Sections  <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">


                                    <li> 
                                        
                                        <a href= "<?php echo base_url('HeadController/section_lists'); ?>">  <i class = 'fa fa-list'> </i> Sections Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('index.php/LoginController/logout'); ?>">Log- Out</a></li>

                    </ul>

                </div>

                <!-- /.sidebar-collapse -->
            </div>
        <?php 

        }
        else{?>

            <div class="navbar-header" >
            
                <a class="navbar-brand" href="<?php echo base_url(''); ?>"> DENR DOCUMENT TRACKER </a>

                <p class="navbar-brand"> (Login As:  Admin) </p>

    </div>


            <div class="navbar-default sidebar" role="navigation">

                <div class="sidebar-nav navbar-collapse" >

                    <ul class="nav" id="side-menu" >


                        <li><a href = "<?php echo base_url('index.php/AdminController/dashboard'); ?>"> Dashboard </a></li>


                        <li><a href = "<?php echo base_url('index.php/AdminController/department'); ?>">  Document Lists <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">

                                    <li> 
                                        <a href= "<?php echo base_url('index.php/AdminController/add_document_lists'); ?>"> <i class = 'fa fa-plus'> </i> Add Document 
                                        </a> 

                                    </li>

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('index.php/AdminController/document_lists'); ?>">  <i class = 'fa fa-list'> </i> Documents Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>


                        <li><a href = "<?php echo base_url('index.php/AdminController/department'); ?>">  Departments  <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">

                                    <li> 
                                        <a href= "<?php echo base_url('index.php/AdminController/add_document_lists'); ?>"> <i class = 'fa fa-plus'> </i> Add Department 
                                        </a> 

                                    </li>

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('index.php/AdminController/departments_lists'); ?>">  <i class = 'fa fa-list'> </i> Departments Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('index.php/AdminController/department'); ?>">  Sections  <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">

                                    <li> 
                                        <a href= "<?php echo base_url('index.php/AdminController/add_document_lists'); ?>"> <i class = 'fa fa-plus'> </i> Add Section 
                                        </a> 

                                    </li>

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('index.php/AdminController/section_lists'); ?>">  <i class = 'fa fa-list'> </i> Sections Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('index.php/LoginController/logout'); ?>">Log- Out</a></li>

                    </ul>

                </div>

                <!-- /.sidebar-collapse -->
            </div>
                 <?php }?>