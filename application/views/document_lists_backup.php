 
<script type="text/javascript">
	
	$("document").ready(function(){
		
		$("#doc_master_list").DataTable();
		
		$(".forward_back_dept_modal_btn").click(function(){

				// alert($(this).attr('value'));

				$("#forward_back_dept_doc_entry_id").val($(this).attr('value'));

			});

		$(".forward_back_helpdesk_modal_btn").click(function(){

				// alert($(this).attr('value'));

				$("#forward_back_helpdesk_doc_entry_id").val($(this).attr('value'));

			});


		});
 

</script>

 <h1 class="page-header"> Document Master Lists</h1>

 <a href="" data-toggle = "modal" data-target = '#add_entry_modal' class="btn btn-primary"> <i class='fa fa-plus'></i> Add Document Entry </a>
 
 <br><br><br>

 <select class="form-control">
 		
 		<option> All Lists </option>
 		<option> Signed </option>
  		<option> Unsigned </option>

 </select>

<br><br>

 <div class = "row" >

 	<table class="table table-striped" id = "doc_master_list" style="width: 100%; ">
                                          
			<thead>

				<tr>
					<td> Document Id 
					<td> Subject 
					<td> First Name 
					<td> Last Name 
					<td> Client Address 
					<td> Phone Number
					<td> Document Date Received
					<td> Sign Status
					<td> Document Status
					<td> Date Returned
					<td> Notes
					<td> Action

			</thead>

			<tbody>

				<?php foreach($entry_list as $row): ?>

					<tr>

						<td> <?php echo $row['doc_entry_id']; ?> 

						<td> <?php echo $row['doc_subject']; ?>

						<td> <?php echo $row['last_name']; ?>

						<td> <?php echo $row['first_name']; ?> 

						<td> <?php echo $row['client_addr']; ?> 

						<td> <?php echo $row['phone_number']; ?> 
 
						<td> <?php echo date("M d, Y", strtotime($row['date_received'])); ?> 

 						<td> <?php echo $row['signed_status']; ?>

						<td> <?php echo $row['document_status']; ?>

						<td> <?php echo date("M d, Y", strtotime($row['date_returned'])); ?> 

						<td> <?php echo $row['notes']; ?> 
 
 						<td>  
  					
  						<?php if($row['document_status_id'] == RECEIVED_HELP_DESK): ?>

							<a href="<?php echo base_url('AdminController/forward_document_to_department/'.$row['doc_entry_id']); ?>" class="btn btn-success"> 

									Forwarded to Department <i class="fa fa-mail-forward"> </i> 

							</a>
						
						<?php endif; ?>

  						<?php if($row['document_status_id'] == FORWARDED_DEPARTMENT): ?>


							<a href="<?php echo base_url('AdminController/forward_document_to_section/'.$row['doc_entry_id']); ?>" class="btn btn-success"> 

								Forwarded to Section <i class="fa fa-mail-forward"> </i>

							</a>

						<?php endif; ?>

  						<?php if($row['document_status_id'] == FORWARDED_SECTION): ?>

							<a href="#" value = "<?php echo $row['doc_entry_id'] ?>"  data-toggle = "modal" data-target = '#forward_back_department_modal' class="forward_back_dept_modal_btn btn btn-success"> 

								Forward back to Department <i class="fa fa-mail-reply"> </i> 

							</a>
						
						<?php endif; ?>

  						<?php if($row['document_status_id'] == RECEIVED_BACK_DEPARTMENT): ?>

							<a href="#" value = "<?php echo $row['doc_entry_id'] ?>" data-toggle = "modal" data-target = '#forward_back_helpdesk_modal' class="forward_back_helpdesk_modal_btn btn btn-success"> 

								Received back by Helpdesk <i class="fa fa-mail-reply"> </i> 

							</a>
						
						<?php endif; ?>

  						<?php if($row['document_status_id'] == RECEIVED_BACK_HELPDESK): ?>
						
						<?php endif; ?>
						
						<br><br>

							<a href=""  data-toggle = "modal" data-target = "#edit_data_entry_modal<?php echo $row['doc_entry_id']; ?>"  class="btn btn-success"> 
								<i class="fa fa-edit"></i> 
							</a>

							<a href="" class="btn btn-danger"> <i class="fa fa-trash"></i>  </a>
						
						<?php $row['section_list']	=	$section_list; ?>

						<?php $this->load->view("modals/edit_entry_modal", $row); ?>
	  				
				<?php endforeach; ?>

 			</tbody>

	</table>               

</div>
 
<?php 
	
	$add_entry_moda_data['section_list']	=	$section_list;

?>

<?php $this->load->view("modals/add_entry_modal", $add_entry_moda_data); ?>

<?php $this->load->view("modals/forward_back_department_modal"); ?>

<?php $this->load->view("modals/forward_back_helpdesk_modal"); ?>

