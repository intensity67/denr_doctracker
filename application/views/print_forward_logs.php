
<link rel="stylesheet" href="<?php echo base_Url('assets/print/print-page.css')  ?>" type="text/css" media="print" />

<link rel="stylesheet" href="<?php echo base_Url('assets/print/print.css')  ?>" type="text/css" media="print" />
    <style type="text/css">
        @media print {
            @page {
                size: 18in 8.5in;
            }
        }
    </style>


    <div class="main-container-wrapper">

                 <div class="table-responsive margin-bottom-20 margin-top-20">

                            <table class="table table-striped table-bordered table-hover table-ecms">
                                <thead>
                                <tr class="top-header">
                                    <th colspan="9">Transaction Information</th>
                                    <th colspan="4">Engine Oil (15W40)</th>
                                    <th colspan="4">Hydraulic Oil (AW100)</th>
                                    <th colspan="4">Gear Oil</th>
                                    <th colspan="4">Telluse</th>
                                </tr>
                                <tr class="default-header">
                                    <!-- <th rowspan="2">Transaction ID</th> -->
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">Vendor</th>
                                    <th rowspan="2">Reference No.</th>
                                    <th rowspan="2">Location</th>
                                    <th rowspan="2">Equipment</th>
                                    <th rowspan="2">Operator</th>
                                    <th rowspan="2">Project</th>
                                    <th rowspan="2">Remarks</th>

                                    <th colspan="2">Qty in Liters</th>
                                    <th rowspan="2">Total Consumption <br/>per Unit</th>
                                    <th rowspan="2">Balance</th>

                                    <th colspan="2">Qty in Liters</th>
                                    <th rowspan="2">Total Consumption <br/>per Unit</th>
                                    <th rowspan="2">Balance</th>

                                    <th colspan="2">Qty in Liters</th>
                                    <th rowspan="2">Total Consumption <br/>per Unit</th>
                                    <th rowspan="2">Balance</th>

                                    <th colspan="2">Qty in Liters</th>
                                    <th rowspan="2">Total Consumption <br/>per Unit</th>
                                    <th rowspan="2">Balance</th>
                                </tr>
                                <tr class="bottom-header">
                                    <!--Engine Oil -->
                                    <th>IN</th>
                                    <th>OUT</th>

                                    <!--Hydraulic Oil -->
                                    <th>IN</th>
                                    <th>OUT</th>

                                    <!--Gear Oil -->
                                    <th>IN</th>
                                    <th>OUT</th>

                                    <!--Tellus Oil -->
                                    <th>IN</th>
                                    <th>OUT</th>
                                </tr>


                                </thead>

                                <tbody>

       						<tfoot>


                                <tr class="total-stock lubricant">
                                    <td colspan="12" class="text-right">Total Engine Oil stock:</td>
 
                                    <td colspan="3" class="text-right">Total Hydraulic Oil stock:</td>
 
                                    <td colspan="3" class="text-right">Total Gear Oil stock:</td>
 
                                    <td colspan="3" class="text-right">Total Telluse stock:</td>
                                 </tr>


                                <tr class="total-consumption lubricant">

                                    <td colspan="12" class="text-right">Total Engine Oil consume:</td>
 
                                    <td colspan="3" class="text-right">Total Hydraulic Oil consume:</td>
 
                                    <td colspan="3" class="text-right">Total Gear Oil consume:</td>
 
                                    <td colspan="3" class="text-right">Total Telluse consume:</td>
                                 
                                 </tr>

                             </tfoot>

         					</table>

                        </div>



        <div class="row">
            <div class="col-md-12">
                <a onClick="window.print()" class="hanging-print btn hidden-print">Print</a>
            </div>