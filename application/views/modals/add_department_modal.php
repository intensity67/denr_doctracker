<div id="add_department_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"> Add Department </h4>
                  </div>
                  <form action = "<?php echo base_url('AdminController/add_department'); ?>" method="POST">
                  <div class="modal-body">
                    <table class="table table-striped">  
                            <tbody>
                              <tr><td> Department Name:        <td> <input type="text" class="form-control" name = "department_name" >
                              <tr><td> Department Description: <td> <input type="text" class="form-control" name = "department_description">   
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value = "Add Department"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>
              
            </div> 