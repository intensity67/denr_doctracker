<div id="edit_data_entry_modal<?php echo $doc_entry_id; ?>" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Edit Document Entry </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/update_document_entry'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>

                              <tr><td> Document Subject: <td> <input type="text" class="form-control" name = "doc_subject" value="<?php echo $doc_subject; ?>" >

                              <tr><td> Client's First Name: <td> <input type="text" class="form-control" name = "first_name" value="<?php echo $first_name; ?>" >

                              <tr><td> Client's Last Name: <td> <input type="text" class="form-control" name = "last_name"  value="<?php echo $last_name; ?>" >
                             
                              <tr><td> Client Address: <td> <input type="text" class="form-control"  value="<?php echo $client_addr; ?>" name = "client_addr" 	>
  
                              <tr><td> Phone Number: <td> <input type="text" class="form-control"  value="<?php echo $phone_number; ?>" name = "phone_number" >

                              <tr><td> Date Received: <td> <input type="date" class="form-control" name = "date_received" 	value="<?php echo date('m/d/Y', strtotime($date_received)); ?>" >
                                
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value="Update Entry"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>

</div> 