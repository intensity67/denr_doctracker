<div id="doc_forward_logs<?php echo $doc_entry_id; ?>" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Document History </h4>
                    <div class="success-confirmation"></div>

                  </div>

                  <div class="modal-body">

                    <table class="table table-striped">
                          
                      <thead>

                        <tr>
                          <td>Forwarded Location
                          <td>Document Passed By
                          <td>Forward Remarks
                          <td>Forwarded Date

                      </thead>

                      <tbody>
                      
                        <?php foreach($doc_forward_logs as $row): ?>                        
                            
                              <tr>
                                  <td><?php echo $row['document_location']; ?>
                                  <td><?php echo $row['acct_name'] . '( '. $row['account_type'] . ' )'; ?>
                                  <td><?php echo $row['forward_remarks']; ?>
                                  <td> <?php echo date("M d, Y", strtotime($row['timestamp'])); ?> 

                        <?php endforeach; ?>

                      </tbody>

                    </table>

                   </div>
                  

                  <div class="modal-footer">
                      <form id="confirmation_form" action="<?php echo base_url('HeadController/update_confirmation'); ?>">
                        <input type="hidden" name="doc_id">
                        <input type="hidden" name="confirmation_status">
                        <a class="btn btn-info update_status" > <i class="fa fa-gear"></i> Confirm </a>
                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                  </div>

                </div>

              </div>

</div> 