<div id="forward_back_department_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Forward Back to Department </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/received_back_department'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>

                              <tr><td> Signed : <td> <input type="checkbox" checked name= "signed" >

                              <tr><td> Signed By: <td> <input type="text"   name= "signed_by" >
                            
                              <tr><td> Date Returned: <td> <input type="date" class="form-control" name = "date_returned" 	>
  
                              <tr><td> Notes: <td> <textarea class="form-control" name = "notes" style="resize: none;" ></textarea>
 
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value="Forward Back to Department"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                      <input type="hidden"  id = "forward_back_dept_doc_entry_id" name="document_id">

                  </div>

                  </form>

                </div>

              </div>

</div> 