<div id="forward_to_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Forward Document </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/forward_document'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>
 
                              <tr><td> Select Destination : 
                                      <td> <select  class="form-control" name="document_location">

                                            <?php foreach($document_locations as $row): ?>

                                                  <option value="<?php echo $row['document_location_id']; ?>"><?php echo $row['document_location']; ?></option>

                                            <?php endforeach; ?>

                                            </select>

                              <tr><td> Remarks: <td> <input type="text" class="form-control" name = "document_remarks">
  
                            </tbody>
                          
                            <input type="hidden" id = "forward_doc_id"  name="document_id">

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-primary" value="Forward Document">

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                  </div>

                  </form>

                </div>

              </div>

</div> 