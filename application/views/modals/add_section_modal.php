<div id="add_section_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Add Section </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/insert_document_entry'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>

                              <tr><td> Section Name:        <td> <input type="text" class="form-control" name = "client_name" >
                            
                              <tr><td> Section Description: <td> <input type="text" class="form-control" name = "client_addr"   >

                              <tr><td> Section Department: <td> 

                                    <select  class="form-control">

                                          <?php foreach($departments_lists as $row): ?>

                                              <option value="<?php echo $row['department_id']; ?>"> <?php echo $row['department_name']; ?> </option>

                                          <?php endforeach; ?>

                                    </select>
                          
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value="Add Section"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>
            </div> 