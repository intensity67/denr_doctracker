<div id="add_account_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Add Account</h4>
                    <div class="validate"></div>
                  </div>

                  <form id="adduserform" action = "<?php echo base_url('AdminController/insert_account'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>

                              <tr><td> Account Name <td> <input type="text" class="form-control" name = "acct_name" ></tr>
                              
                              <tr><td> Username <td> <input type="text" class="form-control" name = "acct_username" ></tr>

                              <tr><td> Account Type <td>
                                  <select  class="form-control" name ="account_type">
                                    <option value="">Please Select User Type...</option>
                                  <?php foreach($user_type as $row): ?>
                                      <option value = "<?php echo $row['account_types_id']; ?>"> <?php echo $row['account_type']; ?></option>
                                  <?php endforeach; ?>   
                                  </select><tr>  
                              <tr><td> Account Type <td>
                                  <select  class="form-control" name ="department_name">
                                  <option value="">Please Select Department...</option>
                                  <?php foreach($departments_lists as $row): ?>
                                      <option value = "<?php echo $row['department_id']; ?>"> <?php echo $row['department_name']; ?></option>
                                  <?php endforeach; ?>   
                                  </select>
                                  <tr>  
                               
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" id="add_account" value="Add User"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>

</div> 