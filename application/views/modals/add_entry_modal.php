<div id="add_entry_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Add Document Entry </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/insert_document_entry'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>

                              <tr><td> Document Subject: <td> <input type="text" class="form-control" name = "doc_subject" >
                             
                              <tr><td> Corporation: <td> <input type="text" class="form-control" name = "corporation" >

                              <tr><td> Document #: <td> <input type="text" class="form-control" name = "doc_no" >

                              <tr><td> Client's First Name: <td> <input type="text" class="form-control" name = "first_name" >

                              <tr><td> Client's Middle Name: <td> <input type="text" class="form-control" name = " middle_name" >
                                
                              <tr><td> Client's Last Name: <td> <input type="text" class="form-control" name = "last_name" >
                            
                              <tr><td> Client Address: <td> <input type="text" class="form-control" name = "client_addr">
  
                              <tr><td> Phone Number: <td> <input type="text" class="form-control" name = "phone_number" >

                              <tr><td> Date Received: <td> <input type="date" class="form-control" name = "date_received" 	>

                             <tr><td> Forwarded Section: <td> 


                                      <select  class="form-control" name ="forwarded_sec_id">

                                          <?php $dept_id = 0; ?>

                                          <?php foreach($section_list as $row): ?>
 
                                            <?php if($dept_id != $row['department_id']): ?>

                                              </optgroup>

                                              <optgroup label="<?php echo $row['department_name']; ?> Department">

                                            <?php   $dept_id = $row['department_id'];

                                                    endif; 
                                              ?>

                                              <option value = "<?php echo $row['section_id']; ?>"> <?php echo $row['section_name']; ?> </option>



                                          <?php endforeach; ?>
                                         
                                      </select>   
                               
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value="Add Entry"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>

</div> 