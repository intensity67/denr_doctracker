<div id="doc_forward_logs<?php echo $doc_entry_id; ?>" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Document History </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/update_document_entry'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                          
                      <thead>

                        <tr>
                          <td>Forwarded Location
                          <td>Document Passed By
                          <td>Forward Remarks
                          <td>Forwarded Date

                      </thead>

                      <tbody>
                      
                        <?php foreach($doc_forward_logs as $row): ?>                        
                            
                              <tr>
                                  <td><?php echo $row['document_location']; ?>
                                  <td><?php echo $row['acct_name'] . '( '. $row['account_type'] . ' )'; ?>
                                  <td><?php echo $row['forward_remarks']; ?>
                                  <td> <?php echo date("M d, Y", strtotime($row['timestamp'])); ?> 

                        <?php endforeach; ?>

                      </tbody>

                    </table>

                   </div>
                  

                  <div class="modal-footer">

                      <form>

                         <a href = '<?php echo base_url('AdminController/print_forward_logs/'.$doc_entry_id); ?>' class="btn btn-info" > <i class="fa fa-print"></i> Print </a>

                       </form>
                       
                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>

</div> 