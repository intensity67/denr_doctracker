<div id="forward_back_helpdesk_modal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Forward Back to Helpdesk </h4>

                  </div>

                  <form action = "<?php echo base_url('AdminController/received_back_helpdesk'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>
 
                              <tr><td> Date Returned: <td> <input type="date" class="form-control" name = "date_returned" 	>
  
                            </tbody>
                          
                            <input type="hidden" id = "forward_back_helpdesk_doc_entry_id" name="document_id">

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value="Forward Back to Helpdesk"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                  </div>

                  </form>

                </div>

              </div>

</div> 