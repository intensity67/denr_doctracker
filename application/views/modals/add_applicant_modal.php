

<div id="addTaskmodal" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Task</h4>
                  </div>

                  <form action="" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody><tr><td>Task Name: </td><td> <input type="text" class="form-control" name="" id="task_name">
                            </td></tr><tr><td>Task Description: </td><td> <input type="text" class="form-control" name="" id="task_desc">
                            </td></tr><tr><td>Task Points: </td><td> <input type="number" class="form-control" name="" id="task_points">
                            </td></tr><tr><td>Initial Progress: </td><td> 

                                                        <div class="form-control"> 
                                                              <select name="" style="width: 90%;" id="init_progress"> 
                                                            
                                                            </select> %

                                                          </div>

                            </td></tr><tr><td>Priority Level:  </td><td> 
                              
                              <select name="priority_level" class="form-control" id="priority_level">

                                <option value="Low">Low</option>
                                <option value="Medium">Medium</option>
                                <option value="High">High</option>
                                                         
                              </select>   
                              
                             </td></tr><tr><td>Skills Required:     
                                </td><td>

                    </td></tr></tbody></table>

                      <input type="hidden" name="project_id" id="project_id" value="1"> 

                  </div>
                  

                  <div class="modal-footer">

                      <input type="button" class="btn btn-success" value="Add Task" name="" id="submit_task_btn">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>
            </div>