 
<script type="text/javascript">
	
	$("document").ready(function(){
		
		var table = $("#doc_master_list").DataTable();
		
		$("#date_from, #date_to").change(function(){

				if($("#date_from").val() != '' && $("#date_to").val() != '') {
				$( "#print_list").unbind( "click" );
					$.post("get_documents_filter", 
							{ 
								dateFrom: $("#date_from").val(), 
								dateTo: $("#date_to").val() }, 

								function(data, status){
								
 
								$("#docu_body").html(data);

							});
 
 				}
		})
 		
 		$('.name_filter').on( 'change', function () {
				 
 			// 	var first_name_filter 	= $("#first_name_filter").val();
				// var middle_name_filter 	= $("#middle_name_filter").val();
				// var last_name_filter 	= $("#last_name_filter").val();
 
 				table
				    .column( $(this).attr("id") )
				    .search(  $(this).val() )
				    .draw();
		
		});


		$(".forward_back_dept_modal_btn").click(function(){

				// alert($(this).attr('value'));

				$("#forward_back_dept_doc_entry_id").val($(this).attr('value'));

			});

		$(".forward_back_helpdesk_modal_btn").click(function(){

				// alert($(this).attr('value'));

				$("#forward_back_helpdesk_doc_entry_id").val($(this).attr('value'));

			});
			
		$(".forward_to_modal_btn").click(function(){
					
				$("#forward_doc_id").val($(this).attr('value'));
		
			});


		});
 

</script>

 <h1 class="page-header"> Document Master Lists</h1>

 <a href="" data-toggle = "modal" data-target = '#add_entry_modal' class="btn btn-primary"> <i class='fa fa-plus'></i> Add Document Entry </a>
 
 <br><br><br>
<form id="print_doc_form" action="<?php echo base_url('AdminController/print_document_list'); ?>" method="Post">
	<table class="table add_entry">

		<tr><td>Date From: <td><input type="date" name="dateFrom" id = "date_from" >
		<tr><td>Date To:   <td><input type="date" name="dateTo" id = "date_to" >
		<tr><td>First Name: <td><input type="text" name="" id = "3" class="name_filter">
		<tr><td>Middle Name: <td><input type="text" name="" id = "4"  class="name_filter">
		<tr><td>Last Name: <td><input type="text" name="" id = "5"  class="name_filter">
	
	</table>

      <div class="col-xs-1 pull-left">
        <input type="submit" class="btn btn-primary btm-sm" id="print_list" value="Print">   
     </div>   

 </form>

	<br><br>

 	<div class = "row" >

 	<table class="table table-striped" id = "doc_master_list" style="width: 100%; ">
                                          
			<thead>

				<tr>
					<td> Document Id 
					<td> Subject 
					<td> Document Number
					<td> Document Date Received
					<td> First Name 
					<td> Middle Name 
					<td> Last Name 
					<td> Client Address 
					<td> Phone Number
					<td> Corporation
					<td> Remarks
					<td> Document Location
 					<td> Action

			</thead>

			<tbody id = "docu_body">

				<?php foreach($doc_entry_list as $row): ?>

					<tr>

						<td> <?php echo $row['doc_entry_id']; ?> 

						<td> <?php echo $row['doc_subject']; ?>
						<td> <?php echo $row['doc_no']; ?>

						<td> <?php echo date("M d, Y", strtotime($row['date_received'])); ?> 

						<td> <?php echo $row['last_name']; ?>

						<td> <?php echo $row['middle_name']; ?> 

						<td> <?php echo $row['first_name']; ?> 

						<td> <?php echo $row['client_addr']; ?> 

						<td> <?php echo $row['phone_number']; ?> 
						<td> <?php echo $row['corporation']; ?> 
 
 						<td> <?php echo $row['signed_status']; ?>

						<td> <?php echo $row['document_location']; ?>

  
 						<td>  
  					     <?php if($row['confirmation_status'] == 0) {?>
 
							<a href="#" class="btn btn-primary forward_to_modal_btn" data-toggle = "modal" data-target = '#forward_to_modal'  value= "<?php echo $row['doc_entry_id']; ?>"> 

									Forwarded Document <i class="fa fa-mail-forward"> </i> 

							</a>
						  <?php }
        					else{?>
        					<a href="javascript:;" class="btn btn-success forward_to_modal_btn" data-toggle = "modal"> 

									UnForwarded Document <i class="fa fa-mail-forward"> </i> 

							</a>
        					<?php }?>
							<a href="#" class="btn btn-success forward_to_modal_btn btn-info btn-mini" data-toggle = "modal" data-target = "#doc_forward_logs<?php echo $row['doc_entry_id']; ?>"  value= "<?php echo $row['doc_entry_id']; ?>"> 

									Forward Logs <i class="fa fa-eye"> </i> 

							</a>
							
 						
						<br><br>

							<a href="#"  data-toggle = "modal" data-target = "#edit_data_entry_modal<?php echo $row['doc_entry_id']; ?>" class="btn btn-success"> 
								<i class="fa fa-edit"></i> 
							</a>

							<a href="" class="btn btn-danger"> <i class="fa fa-trash"></i>  </a>
						
						<?php $row['section_list']	=	$section_list; ?>

						<?php $this->load->view("modals/edit_entry_modal", $row); ?>

						<?php $doc_history_modal_data['doc_forward_logs']	=	$this->documentModel->get_doc_forward_logs($row['doc_entry_id']); ?>

						<?php $this->load->view("modals/doc_history_modal", $doc_history_modal_data); ?>
	  				
				<?php endforeach; ?>

 			</tbody>

	</table>               

</div>
 
<?php 
	
	$add_entry_moda_data['section_list']	=	$section_list;
	$forward_to_modal['document_locations']	=	$document_locations;
	
 ?>

<?php $this->load->view("modals/add_entry_modal", $add_entry_moda_data); ?>

<?php $this->load->view("modals/forward_to_modal", $forward_to_modal); ?>

