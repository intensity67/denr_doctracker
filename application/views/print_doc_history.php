<?php
	
	require 'fpdf/fpdf.php';

	class PDF extends FPDF{

	public $requested_by_id, $requested_by_name, $requested_by_date;
	 

	function Header() {
  
			//Logo

			 //Arial bold 15
			$this->SetFont('Arial','B',15);
			//Move to the right
			$this->Cell(80);

			$this->Cell(30, 80, 'DOCUMENT HISTORY LOGS' ,0,0,'C');

 			$this->Ln(30);
			$this->Cell(30,45,'Document: 12313' ,0,0,'C');

 			$this->Ln(20);


 			$this->Ln(20);

			$this->Cell(30,45,'Date: '. date("M, d Y") ,0,0,'C');

 			
			//Line break
			$this->SetFont('Arial','B',8);
			$this->Ln(20);
			
 			$this->Ln(20);

	} 
 
		 

		// Simple table	enrollees
		function BasicTable($data)
		{
		
		   $this->Cell(20,7,"#",1, 0,'C');
		   $this->Cell(40,7,"Document Location",1, 0,'C');
		   $this->Cell(40,7,"Account Name", 1, 0,'C');
		   $this->Cell(40,7,"Forward Remarks", 1, 0,'C');
		   $this->Cell(40,7,"Timestamp", 1, 0,'C');

			$this->Ln();
			  

 		$this->SetFont('Arial','', 7);

		for($i= 0; isset($data[$i]); $i++) //* Display Rows of the Table
			{
			
 				$this->Cell(20,6,$data[$i]['nos'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['document_location'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['acct_name'],1, 0,'C');
  				$this->Cell(40,6,$data[$i]['forward_remarks'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['timestamp'],1, 0,'C');
				$this->Ln();
			} 
			
			  	//* Display Columns of the Table
				 
 		}

		// Page footer
		function Footer()
		{
			// Position at 1.5 cm from bottom
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',8);
			// Page number
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
  
	}


		$pdf = new PDF(); 		

		$pdf->AddPage();

 		$i= 0;
	
				foreach( $doc_forward_logs as $row){

 					$reports_row[$i]['nos'] =	$i + 1;

 					$reports_row[$i]['document_location']	=	$row['document_location'];
 					$reports_row[$i]['acct_name'] 			=	$row['acct_name'];
 					$reports_row[$i]['forward_remarks'] 	=	$row['forward_remarks'];
 					$reports_row[$i]['timestamp'] 			= date("M d, Y", strtotime($row['timestamp']));
					
					$i++;
				
				}
 
			
		//*For Background: $pdf->Image('img/background.png',0,15,-175);
		
		if(!empty($reports_row))

			$pdf->BasicTable($reports_row);
		else 

			$pdf->EmptyInventory();

		$pdf->SetFont('Arial','', 7);

		$pdf->Cell(0,10,'Date Printed:  '. date("M d Y"));
 
		$pdf->SetTitle('Inventory Report');

		$pdf->Output();


?>