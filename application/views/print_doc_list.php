<?php
	
	require 'fpdf/fpdf.php';

	class PDF extends FPDF{

	public $requested_by_id, $requested_by_name, $requested_by_date;
	 

	function Header() {
  
			//Logo

			 //Arial bold 15
			$this->SetFont('Arial','B',15);
			//Move to the right
			$this->Cell(150);

			$this->Cell(30, 80, 'DOCUMENT LIST ENTRY' ,0,0,'C');


 			$this->Ln(20);


 			$this->Ln(20);

			$this->Cell(30,45,'Date: '. date("M, d Y") ,0,0,'C');

 			
			//Line break
			$this->SetFont('Arial','B',8);
			$this->Ln(20);
			
 			$this->Ln(20);

	} 
 
		 

		// Simple table	enrollees
		function BasicTable($data)
		{
		
		   $this->Cell(8,7,"#",1, 0,'C');
		   $this->Cell(25,7,"Subject",1, 0,'C');
		   $this->Cell(30,7,"Document Number", 1, 0,'C');
		   $this->Cell(40,7,"Ducument Date Recieved", 1, 0,'C');
		   $this->Cell(25,7,"First Name", 1, 0,'C');
		   $this->Cell(25,7,"Middle Name", 1, 0,'C');
		   $this->Cell(25,7,"Last Name", 1, 0,'C');
		   $this->Cell(40,7,"Client Address", 1, 0,'C');
		   $this->Cell(25,7,"Phone Number", 1, 0,'C');
		   $this->Cell(25,7,"Corporation", 1, 0,'C');
		   $this->Cell(25,7,"Remarks", 1, 0,'C');
		   $this->Cell(30,7,"Document Location", 1, 0,'C');

			$this->Ln();
			  

 		$this->SetFont('Arial','', 7);

		for($i= 0; isset($data[$i]); $i++) //* Display Rows of the Table
			{
			
 				$this->Cell(8,6,$data[$i]['nos'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['doc_subject'],1, 0,'C');
 				$this->Cell(30,6,$data[$i]['doc_no'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['date_received'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['first_name'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['middle_name'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['last_name'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['client_addr'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['phone_number'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['corporation'],1, 0,'C');
 				$this->Cell(25,6,$data[$i]['signed_status'],1, 0,'C');
 				$this->Cell(30,6,$data[$i]['document_location'],1, 0,'C');
				$this->Ln();
			} 
			
			  	//* Display Columns of the Table
				 
 		}

		// Page footer
		function Footer()
		{
			// Position at 1.5 cm from bottom
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',8);
			// Page number
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
  
	}


	$pdf = new PDF('P','mm',array(340,540));	

		$pdf->AddPage();

 		$i= 0;
	
				foreach($doc_entry_list as $row){

 					$reports_row[$i]['nos'] =	$i + 1;
 					$reports_row[$i]['doc_subject']	=	$row['doc_subject'];
 					$reports_row[$i]['doc_no']	=	$row['doc_no'];
 					$reports_row[$i]['date_received']	=	date("M d, Y", strtotime($row['date_received']));
 					$reports_row[$i]['first_name'] 	= $row['first_name'];
 					$reports_row[$i]['middle_name'] =	$row['middle_name'];
 					$reports_row[$i]['last_name'] 	=	$row['last_name'];
 					$reports_row[$i]['client_addr'] = $row['client_addr'];
 					$reports_row[$i]['phone_number'] = $row['phone_number'];
 					$reports_row[$i]['corporation'] 	= $row['corporation'];
 					$reports_row[$i]['signed_status'] 	= $row['signed_status'];
 					$reports_row[$i]['document_location'] 	= $row['document_location'];
					
					$i++;
				
				}
 
			
		//*For Background: $pdf->Image('img/background.png',0,15,-175);
		
		if(!empty($reports_row))
			$pdf->BasicTable($reports_row);
		else 

		$pdf->SetFont('Arial','', 7);

		$pdf->Cell(0,10,'Date Printed:  '. date("M d Y"));
 
		$pdf->SetTitle('Inventory Report');

		$pdf->Output();


?>