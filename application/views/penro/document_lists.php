 
<script type="text/javascript">
	
	$("document").ready(function(){
		
		var table = $("#doc_master_list").DataTable();
		
		$("#date_from, #date_to").change(function(){

				if($("#date_from").val() != '' && $("#date_to").val() != '') {
					$.post("get_documents_filter", 
							{ 
								dateFrom: $("#date_from").val(), 
								dateTo: $("#date_to").val() }, 

								function(data, status){
								$("#docu_body").html(data);

							});
 
 				}
		})
 		
 		$('.name_filter').on( 'change', function () {
 				table
				    .column( $(this).attr("id") )
				    .search(  $(this).val() )
				    .draw();
		
		});

		$(".forward_back_dept_modal_btn").click(function(){

				// alert($(this).attr('value'));

				$("#forward_back_dept_doc_entry_id").val($(this).attr('value'));

			});

		$(".forward_back_helpdesk_modal_btn").click(function(){

				// alert($(this).attr('value'));

				$("#forward_back_helpdesk_doc_entry_id").val($(this).attr('value'));

			});
			
		$(".forward_to_modal_btn").click(function(){
					
				$("#forward_doc_id").val($(this).attr('value'));
		
			});


		});
 

</script>

 <h1 class="page-header"> Document Master Lists</h1>


 <br><br><br>
	<table class="table add_entry">

		<tr><td>Date From: <td><input type="date" name="dateFrom" id = "date_from" ></td>
		<tr><td>Date To:   <td><input type="date" name="dateTo" id = "date_to" ></td>
		<tr><td>First Name: <td><input type="text" name="" id = "3" class="name_filter"></td>
		<tr><td>Middle Name: <td><input type="text" name="" id = "4"  class="name_filter"></td>
		<tr><td>Last Name: <td><input type="text" name="" id = "5"  class="name_filter"></td>
	
	</table>
	<br><br>

 	<div class="row" style="overflow:auto">

 	<table class="table table-striped" id = "doc_master_list" >
                                          
			<thead>

				<tr>
					<th> Document Id</th> 
					<th> Subject</th> 
					<th> Document Number</th>
					<th> Document Date Received</th>
					<th> First Name </th>
					<th> Middle Name </th>
					<th> Last Name </th>
					<th> Client Address </th>
					<th> Phone Number</th>
					<th> Corporation</th>
					<th> Remarks</th>
					<th> Document Location</th>
					<th>Status</th>
					<th style="display:none;">Status</th>
 					<th>Action</th>
 				</tr>

			</thead>

			<tbody id = "docu_body">

				<?php foreach($doc_entry_list as $row): ?>

					<tr>
						<td> <?php echo $row['doc_entry_id']; ?> 
						<td> <?php echo $row['doc_subject']; ?>
						<td> <?php echo $row['doc_no']; ?>
						<td> <?php echo date("M d, Y", strtotime($row['date_received'])); ?> 
						<td> <?php echo $row['last_name']; ?>

						<td> <?php echo $row['middle_name']; ?> 

						<td> <?php echo $row['first_name']; ?> 

						<td> <?php echo $row['client_addr']; ?> 

						<td> <?php echo $row['phone_number']; ?> 
						<td> <?php echo $row['corporation']; ?> 
 
 						<td> <?php echo $row['signed_status']; ?>

						<td> <?php echo $row['document_location']; ?>
						<td><?php echo ($row['confirmation_status'] == 1 ? 'Confirmed' : 'Unconfirmed')?> </td>  
						<td style="display:none";><?php echo $row['confirmation_status'] ?></td>  
 						<td> <a href="#" class="btn btn-success view_forward_log forward_to_modal_btn btn-info btn-mini" data-toggle = "modal" data-target = "#doc_forward_logs<?php echo $row['doc_entry_id']; ?>"  value= "<?php echo $row['doc_entry_id']; ?>" data-docid="<?php echo $row['doc_entry_id']; ?>"> 
										Forward Logs <i class="fa fa-eye"> </i> 
							</a>
						<br><br>					
						<?php $row['section_list']	=	$section_list; ?>

						<?php $this->load->view("modals/edit_entry_modal", $row); ?>

						<?php $doc_history_modal_data['doc_forward_logs']	=	$this->documentModel->get_doc_forward_logs($row['doc_entry_id']);?>

						<?php $this->load->view("modals/doc_history_head_modal.php", $doc_history_modal_data); ?>
	  			</tr>
				<?php endforeach; ?>
			

 			</tbody>

	</table>               

</div>
 
<?php 
	
	$add_entry_moda_data['section_list']	=	$section_list;
	$forward_to_modal['document_locations']	=	$document_locations;
	
 ?>


