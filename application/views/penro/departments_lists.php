 
<script type="text/javascript">
	
	$("document").ready(function(){
		
			$("#doc_master_list").DataTable();
		
		});

</script>

 <h1 class="page-header"> Department Lists </h1>
 
<br><br>

 <div class = "row" >

 	<table class="table table-striped" id = "doc_master_list" style="width: 100%; ">
                                          
			<thead>

				<tr>
					<td> Department Id 
					<td> Name of Department 
					<td>  

			</thead>

			<tbody>

				<?php foreach($departments_lists as $row): ?>

					<tr>

						<td> <?php echo $row['department_id']; ?> 

						<td> <?php echo $row['department_name']; ?> 
				 
 						<td>  
  
	  				
				<?php endforeach; ?>

 			</tbody>

	</table>               

</div>
 
 
<?php $this->load->view("modals/add_department_modal"); ?>