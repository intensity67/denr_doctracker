 
<script type="text/javascript">
	
	$("document").ready(function(){
		
			$("#doc_master_list").DataTable();
		
		});

</script>

 <h1 class="page-header"> Section Lists </h1>
 

<br><br>

 <div class = "row" >

 	<table class="table table-striped" id = "doc_master_list" style="width: 100%; ">
                                          
			<thead>

				<tr>
					<td> Section Id 
					<td> Name of Client 

			</thead>

			<tbody>

				<?php foreach($section_list as $row): ?>

					<tr>

						<td> <?php echo $row['section_id']; ?> 

						<td> <?php echo $row['section_name']; ?> 
	  				
				<?php endforeach; ?>

 			</tbody>

	</table>               

</div>
 
<?php 
	
	$add_section_modal_data['departments_lists']	=	$departments_lists;

?>

<?php $this->load->view("modals/add_section_modal", $add_section_modal_data); ?>