 
<script type="text/javascript">
  $(document).ready(function() {
  $('#user_account_list').DataTable({
                responsive: true
        });
    });

</script>

 <h1 class="page-header">  User Account Lists</h1>
 
 <br><br><br>
	

 	<div class = "row" >
 	<table class="table table-striped" id = "user_account_list" style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Account Name </th>
					<th> Username </th>
					<th> Account Type </th>
					<th> Department </th>
					<th> Date Created </th>
				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($user_list as $row): ?>

					<tr>
						<td> <?php echo $row['acct_name']; ?></td> 
						<td> <?php echo $row['acct_username']; ?></td>
						<td> <?php echo $row['account_type']; ?></td>
						<td> <?php echo $row['department_name']; ?></td>
						<td> <?php echo $row['acct_date_created']; ?></td>
					</tr>
	  				
				<?php endforeach; ?>

 			</tbody>

	</table>               
</div>

<?php $this->load->view("modals/add_useraccount_modal");?>
