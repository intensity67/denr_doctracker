 
<script type="text/javascript">
	
	$("document").ready(function(){
		
			$("#doc_master_list").DataTable();
		
		});

</script>

 <h1 class="page-header"> Document Master Lists</h1>

 <a href="" data-toggle = "modal" data-target = '#add_entry_modal' class="btn btn-primary"> <i class='fa fa-plus'></i> Add Document Entry </a>
 
 <br><br><br>

 <select class="form-control">
 		
 		<option> All Lists </option>
 		<option> Signed </option>
  		<option> Unsigned </option>

 </select>

<br><br>

 <div class = "row" >

 	<table class="table table-striped" id = "doc_master_list" style="width: 100%; ">
                                          
			<thead>

				<tr>
					<td> Document Id 
					<td> Name of Client 
					<td> Client Address 
					<td> Phone Number
					<td> Document Date Received
 					<td> Forwarded Section
					<td> Sign Status
					<td> Document Status
					<td> Date Returned
					<td> Notes
					<td> Action
					<td> 
					<td> 

			</thead>

			<tbody>

				<?php foreach($doc_entry_list as $row): ?>

					<tr>

						<td> <?php echo $row['doc_entry_id']; ?> 

 						<td> <?php echo $row['last_name']; ?>

						<td> <?php echo $row['middle_name']; ?> 

						<td> <?php echo $row['first_name']; ?> 

						<td> <?php echo $row['client_addr']; ?> 

						<td> <?php echo $row['phone_number']; ?> 
 
						<td> <?php echo date("M d, Y", strtotime($row['date_received'])); ?> 

						<td> <?php echo $row['section_name']; ?> 

						<td> <?php echo $row['signed_status']; ?>

						<td> <?php echo $row['document_status']; ?>

						<td> <?php echo date("M d, Y", strtotime($row['date_returned'])); ?> 

						<td> <?php echo $row['notes']; ?> 
 
 						<td>  
  	  				
							<a href="#" class="btn btn-success forward_to_modal_btn" data-toggle = "modal" data-target = '#forward_to_modal'  value= "<?php echo $row['doc_entry_id']; ?>"> 

									Forwarded Document <i class="fa fa-mail-forward"> </i> 

							</a>
				<?php endforeach; ?>

 			</tbody>

	</table>               

</div>
 
<?php 

	$add_entry_moda_data['section_list'] 	= 	$section_list; 
	$forward_to_modal['document_locations']	=	$document_locations;

?>

<?php $this->load->view("modals/add_entry_modal", $add_entry_moda_data); ?>

<?php $this->load->view("modals/forward_to_modal", $forward_to_modal); ?>
