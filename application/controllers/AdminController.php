<?php


defined('BASEPATH') OR exit('No direct script access allowed');


class AdminController extends CI_Controller {

	public function __construct(){
 
			parent::__construct();
                // Your own constructor code
 
    }
       
 	public function admin_page_render($page, $data){

			$this->load->view('layout/header');

	        $this->load->view($page, $data);
	       
			$this->load->view('layout/footer');

     }
 

    public function index(){

        redirect("AdminController/document_lists");

    }

    public function print_forward_logs($doc_id){

            $data['page_title']                             =   "Print Documents History Logs";

            $doc_history_modal_data['doc_forward_logs']     =   $this->documentModel->get_doc_forward_logs($doc_id); 

            $page = 'print_doc_history';
            
            $this->load->view($page, $doc_history_modal_data);

    }
    // Print Document List
     public function print_document_list(){
      $date_from              = $this->input->post('dateFrom');
      $date_to                = $this->input->post('dateTo'); 
        $this->form_validation->set_rules('dateFrom','Date From','trim|required|xss_clean');
        $this->form_validation->set_rules('dateTo','Date To','trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == FALSE){
             echo json_encode(array("response" => "error", "message" => validation_errors()));
        }
    else{
            $filters['date_from']   =   $date_from;
            $filters['date_to']     =   $date_to;

            $data['doc_entry_list'] = $this->documentModel->get_entry_lists($filters);
            
            $data['section_list']   = $this->documentModel->get_section_lists();
            
            $page                   = "print_doc_list";

            $this->load->view($page, $data);      
    }


    }
  
 	public function document_lists(){

        	$data['page_title']         = "Document Lists";

        	$data['section_list']       = $this->documentModel->get_section_lists();

        	$data['doc_entry_list']     = $this->documentModel->get_entry_lists();


        	$data['departments_lists']	= $this->documentModel->get_department_lists();

        	$data['document_locations']	= $this->documentModel->get_document_locations();

	        $page = 'document_lists';

            
            $this->admin_page_render($page, $data);

 	}
    public function dashboard(){

            $data['page_title']      = "Account Lists";
            $data['user_list']       = $this->UserModel->get_user_lists();
            $data['user_type']       = $this->UserModel->get_usertype();
            $data['departments_lists']  = $this->documentModel->get_department_lists();
            $page = 'account_lists';

            $this->load->view('layout/header');

             $this->load->view($page, $data);
           
            $this->load->view('layout/footer');

    }


    public function get_documents_filter(){

        $date_from              = $this->input->post('dateFrom');
        $date_to                = $this->input->post('dateTo');
 

        $filters['date_from']   =   $date_from;
        $filters['date_to']     =   $date_to;

        $data['doc_entry_list'] = $this->documentModel->get_entry_lists($filters);
        
        $data['section_list']   = $this->documentModel->get_section_lists();
        
        $page                   = "document_list_values";

        $this->load->view($page, $data);

    }

 	public function insert_document_entry(){

            $document_entry_data['doc_subject']         = $this->input->post('doc_subject');
            $document_entry_data['doc_no']              = $this->input->post('doc_no');
 			$document_entry_data['first_name']			= $this->input->post('first_name');
            $document_entry_data['middle_name']         = $this->input->post('middle_name');
            $document_entry_data['last_name']           = $this->input->post('last_name');
 			$document_entry_data['client_addr'] 		= $this->input->post('client_addr');
            $document_entry_data['phone_number']        = $this->input->post('phone_number');
 			$document_entry_data['corporation']		    = $this->input->post('corporation');
 			$document_entry_data['date_received']		= $this->input->post('date_received');
 			$document_entry_data['forwarded_sec_id'] 	= $this->input->post('forwarded_sec_id');

 			$section_info 								= $this->documentModel->get_section_info($this->input->post('forwarded_sec_id'));

 			$document_entry_data['forwarded_dept_id'] 	= $section_info['department_id'];

 			$document_entry_data['document_status_id'] 	= RECEIVED_HELP_DESK;


 			// Inserts in DB

        	$docu_entry_id								= $this->documentModel->insert_document_entry($document_entry_data);


        	redirect("AdminController/document_lists");
 	}

   public function insert_account(){
    $this->form_validation->set_rules('acct_name','Account Name','trim|required|xss_clean');
    $this->form_validation->set_rules('acct_username','UserName','trim|required|xss_clean|is_unique[accounts.acct_username]', array('required => You have not provide %s.', 'is_unique => this %s already exists'));
    $this->form_validation->set_rules('account_type','Account Type','trim|required|xss_clean');
    $this->form_validation->set_rules('department_name','Department','trim|required|xss_clean');
    if ($this->form_validation->run() == FALSE){
         echo json_encode(array("response" => "error", "message" => validation_errors()));
    }
    else{
    $data = array(
                'acct_username'=> $this->input->post('acct_username'),
                'acct_password' => md5($this->input->post('acct_username')),
                'acct_name' => $this->input->post('acct_name'),
                'acct_type_id' => $this->input->post('account_type'),
                'acct_dept_id' => $this->input->post('department_name'),
                'acct_section_id' => 0,
                'acct_date_created' => date("Y-m-d h:i:s")
                );

                  $this->UserModel->insertaccount($data);
                  echo json_encode(array("response" => "success", "message" => "Successfully added user", "redirect" => base_url('AdminController/dashboard')));
         }
   }

    public function add_department(){
    $this->form_validation->set_rules('department_name','Department Name','trim|required|xss_clean');
    $this->form_validation->set_rules('department_description','Department Decription','trim|required|xss_clean');
    if ($this->form_validation->run() == FALSE){
         echo json_encode(array("response" => "error", "message" => validation_errors()));
    }
    else{
    $data = array(
                'department_name'=> $this->input->post('department_name'),
                'department_desc' => $this->input->post('department_description'),
                'department_status' => 'Active'
                );
                $this->departmentModel->add_departments($data);
                echo json_encode(array("response" => "success", "message" => "Successfully added Department", "redirect" => base_url('AdminController/departments_lists')));
         }
   }

 	public function section_lists(){

        	$data['page_title'] 	= "Document Lists";

        	$data['section_list']	= $this->documentModel->get_section_lists();
        	
        	$data['departments_lists']	= $this->documentModel->get_department_lists();

 	        $page = 'section_lists';

	        $this->admin_page_render($page, $data);
	        
	}

 	public function departments_lists(){

        	$data['page_title'] 		= "Document Lists";

        	$data['departments_lists']	= $this->documentModel->get_department_lists();

 	        $page = 'departments_lists';

	        $this->admin_page_render($page, $data);
	        
	}

    
    public function forward_document(){
            $forward_doc_data['document_location_id']   = $this->input->post('document_location');
            $forward_doc_data['current_remarks']        = $this->input->post('document_remarks');
            $forward_doc_data['for_status']        = 1;
            $document_id                                = $this->input->post('document_id');
            $this->documentModel->forward_document($document_id, $forward_doc_data);
            $forward_doc_log_data['doc_id']              =   $document_id;
            $forward_doc_log_data['doc_location_id']     =   $forward_doc_data['document_location_id'];
            $forward_doc_log_data['forward_remarks']     =   $forward_doc_data['current_remarks'];
            $forward_doc_log_data['passed_by_id']        =   $this->session->userdata('account_info')['acct_id'];
            $this->documentModel->insert_doc_forward_logs( $forward_doc_log_data);
            redirect("AdminController/document_lists");

    }


    public function forward_document_to_department($document_id){
 			 
 			$this->documentModel->forward_document_to_department($document_id);

        	redirect("AdminController/document_lists");

 	}

 	public function forward_document_to_section($document_id){
 			 
 			$this->documentModel->forward_document_to_section($document_id);

        	redirect("AdminController/document_lists");

 	}

 	public function received_back_department(){
 			
 			$document_id 						= $this->input->post('document_id');
 			$updated_fields['signed_status'] 	= 'signed';
 			$updated_fields['signed_by'] 		= $this->input->post('signed_by');
 			$updated_fields['date_returned'] 	= $this->input->post('date_returned');
 			$updated_fields['notes'] 			= $this->input->post('notes');
 			$updated_fields['document_status_id'] = RECEIVED_BACK_DEPARTMENT;
 
 			$this->documentModel->received_back_department($document_id, $updated_fields);

        	redirect("AdminController/document_lists");

 	}

 	public function received_back_helpdesk($document_id){
			
			$updated_fields['date_returned']	= $this->input->post('date_returned');

 			$document_id 						= $this->input->post('document_id');

 			$this->documentModel->received_back_helpdesk($document_id, $updated_fields);

        	redirect("AdminController/document_lists");

 	}

}