<?php


defined('BASEPATH') OR exit('No direct script access allowed');


class ManagementStaffController extends CI_Controller {

	public function __construct(){
 
			parent::__construct();
                // Your own constructor code
 
    }
       
 	public function page_render($page, $data){

			$this->load->view('layout/header');

	        $this->load->view( MANAGEMENT_STAFF_VIEW_DIRECTORY . $page, $data);
	       
			$this->load->view('layout/footer');

     }


 	public function index( ){

        	$data['section_list']       = $this->documentModel->get_section_lists();

        	$data['doc_entry_list']     = $this->documentModel->get_entry_lists();

        	$data['departments_lists']	= $this->documentModel->get_department_lists();

        	$data['document_locations']	= $this->documentModel->get_document_locations();

	        $page = 'management_home';

	        $this->page_render($page, $data);

	}

}