<?php

class LoginController extends CI_Controller {


	public function __construct(){
 
			parent::__construct();
  
    }
      
	public function index(){
		
		$this->load->view('signin_form');

	}

	// Ajax Login Check 

	public function login_check(){

		$username 		= $this->input->post('username');

		$password 		= md5($this->input->post('password'));

		$login_result   = $this->LoginModel->get_users_by_login($username, $password);

		// indicates valid login... 

		if( !empty($login_result) ) {

			$account_data 					= $login_result;

			$session_data['account_info'] 	= $account_data;

			$this->session->set_userdata($session_data);

			if( $login_result['acct_type_id'] == HELP_DESK_STAFF){


				redirect("HelpDeskStaffController/");

			}
			
			if( $login_result['acct_type_id'] == MANAGEMENT_STAFF){

				redirect("ManagementStaffController/");


			}
			
			if( $login_result['acct_type_id'] == TECHNICAL_STAFF){
				
				redirect("TechnicalStaffController/");

			}

			if( $login_result['acct_type_id'] == ADMIN){
				
				redirect("AdminController/");

			}
			if( $login_result['acct_type_id'] == HEAD ){
				
				redirect("HeadController/");

			}


			redirect('AdminController/document_lists');

		} else {

			$this->session->set_userdata("error_login", 1);

			redirect("LoginController/");

		}

	}


	public function logout(){
 		
 		$this->session->unset_userdata('account_session');

		redirect("/");

	}

}

?>