<?php


defined('BASEPATH') OR exit('No direct script access allowed');


class HeadController extends CI_Controller {

	public function __construct(){
 
			parent::__construct();
                // Your own constructor code
 
    }
       
 	public function admin_page_render($page, $data){

			$this->load->view('layout/header');

	        $this->load->view($page, $data);
	       
			$this->load->view('layout/footer');

     }
 

    public function index(){

        redirect("HeadController/document_lists");

    }

 	public function document_lists(){

        	$data['page_title']         = "Document Lists";

        	$data['section_list']       = $this->documentModel->get_section_lists();

        	$data['doc_entry_list']     = $this->documentModel->get_entry_lists();


        	$data['departments_lists']	= $this->documentModel->get_department_lists();

        	$data['document_locations']	= $this->documentModel->get_document_locations();

	        $page = 'penro/document_lists';

            
            $this->admin_page_render($page, $data);

 	}
    public function dashboard(){

            $data['page_title']      = "Account Lists";
            $data['user_list']       = $this->UserModel->get_user_lists();
            $data['user_type']       = $this->UserModel->get_usertype();
            $data['departments_lists']  = $this->documentModel->get_department_lists();
            $page = 'penro/account_lists';

            $this->load->view('layout/header');

             $this->load->view($page, $data);
           
            $this->load->view('layout/footer');

    }


    public function get_documents_filter(){

        $date_from              = $this->input->post('dateFrom');
        $date_to                = $this->input->post('dateTo');
 

        $filters['date_from']   =   $date_from;
        $filters['date_to']     =   $date_to;

        $data['doc_entry_list'] = $this->documentModel->get_entry_lists($filters);
        
        $data['section_list']   = $this->documentModel->get_section_lists();
        
        $page                   = "penro/document_list_values";

        $this->load->view($page, $data);

    }


 	public function section_lists(){

        	$data['page_title'] 	= "Document Lists";

        	$data['section_list']	= $this->documentModel->get_section_lists();
        	
        	$data['departments_lists']	= $this->documentModel->get_department_lists();

 	        $page = 'penro/section_lists';

	        $this->admin_page_render($page, $data);
	        
	}

 	public function departments_lists(){

        	$data['page_title'] 		= "Document Lists";

        	$data['departments_lists']	= $this->documentModel->get_department_lists();

 	        $page = 'penro/departments_lists';

	        $this->admin_page_render($page, $data);
	        
	}

    
    public function forward_document(){

            $forward_doc_data['document_location_id']   = $this->input->post('document_location');
            $forward_doc_data['current_remarks']        = $this->input->post('document_remarks');

            $document_id                                = $this->input->post('document_id');

            $this->documentModel->forward_document($document_id, $forward_doc_data);

            $forward_doc_log_data['doc_id']              =   $document_id;
            $forward_doc_log_data['doc_location_id']     =   $forward_doc_data['document_location_id'];
            $forward_doc_log_data['forward_remarks']     =   $forward_doc_data['current_remarks'];
            $forward_doc_log_data['passed_by_id']        =   $this->session->userdata('account_info')['acct_id'];
 
            $this->documentModel->insert_doc_forward_logs( $forward_doc_log_data );

            redirect("HeadController/document_lists");

    }


    public function forward_document_to_department($document_id){
 			 
 			$this->documentModel->forward_document_to_department($document_id);

        	redirect("HeadController/document_lists");

 	}

 	public function forward_document_to_section($document_id){
 			 
 			$this->documentModel->forward_document_to_section($document_id);

        	redirect("AdminController/document_lists");

 	}

    public function notify_doc_status() { 
       $status = array();
       $notification_status=$this->documentModel->notify_doc();
       $status=$notification_status;
       echo json_encode($status); 
      }
    public function get_status() { 
       $status = $this->input->get('status');
       $this->documentModel->update_status($status);
    }
    public function update_confirmation(){
     $doc_id = $this->input->post('doc_id');
     $confirmation_status = $this->input->post('confirmation_status');
     if ($confirmation_status == 0){
        $this->documentModel->update_confirmation($doc_id, 1);
     }
     else{
       $this->documentModel->update_confirmation($doc_id, 0);
     }

     echo json_encode(array("response" => "success", "message" => "Successfully updated Confirmation", "redirect" => base_url('HeadController')));

    }
}